#!/bin/bash

INST=$(which mysql | grep mysql | wc -l);

if [ $INST -ne 0 ]; then

    UP=$(pgrep mysql | wc -l);

    if [ ! "$UP" -ne 1 ]; then
        mysql.server stop;	
    fi

    brew uninstall mysql;
    rm -rf ~/.my.cnf;

fi


